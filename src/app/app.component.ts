import { Component } from '@angular/core';
import { io } from "socket.io-client";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  broadcastTypes = [ "DEARS","FANS","ALL_LIKERS","ALL_PREMIUM_LIKERS","LIKERS"]
  title = 'socket-io-tester';
  url = '';
  token = ''
  socket = undefined
  message = {
    broadcastType : "DEARS",
    message : ''
  }

  connect(){
    console.log(this.url)
    if(this.url){
      console.log("inside")
      this.socket = io(this.url,{
        transports:["websocket"],
        path:'/messej',
        query:{token:this.token}
      })

      this.socket.on('connect',() => {
        console.log("connected")
        this.socket.emit('testMessage')
      })
      this.socket.on("testMessage", (msg) => {
        console.log(msg)
      });

      this.socket.on("connect_error", (error) => {
        console.log(error)
      });

      this.socket.on("error", (error) => {
        console.log(error)
      });

      this.socket.on('broadcast',(msg) =>{
        console.log(msg)
        this.bindMessage(msg)
      })
    }


  }

  send(){
    this.socket.emit('broadcast',this.message)
  }

 close(){
   this.socket.disconnect()
 }

 bindMessage(message){
     let swatchBox = document.getElementById("output")
     swatchBox.className = "w-50 column";
     let output = document.createElement("div")
     output.className="m-2"
     output.appendChild(document.createTextNode(JSON.stringify(message)));
     swatchBox.appendChild(output)
 }
}
